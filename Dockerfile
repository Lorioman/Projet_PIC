FROM centos:7
RUN yum -y update && yum -y install java-1.8.0-openjdk-devel  java-1.8.0-openjdk  epel-release ansible maven git #wget


RUN export JAVA_HOME=/usr/bin/java
RUN mkdir -p /apps/jenkins
ADD http://mirrors.jenkins.io/war-stable/latest/jenkins.war /apps/jenkins
##RUN wget -qO - https://pkg.jenkins.io/debian/jenkins.io.key | apt-key add 
ENV JAVA_OPTS="-Djenkins.install.runSetupWizard=false"
RUN useradd jenkins
#RUN groupadd jenkins
RUN chown -R jenkins:jenkins /apps/jenkins
RUN echo "jenkins" | passwd --stdin jenkins
#VOLUME  
EXPOSE 8080
CMD exec /usr/bin/java -jar /apps/jenkins/jenkins.war
#CMD ["/script.sh"]
